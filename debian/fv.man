.TH FV "1" "July 2009" "Heasoft" "User Commands"
.SH NAME
fv \- a tool for viewing and editing FITS format files
.SH SYNOPSIS
.B fv
.SH DESCRIPTION
.PP
fv is a FITS file viewer and editor developed at the High Energy Astrophysics
Science Archive Research Center (HEASARC) at NASA / GSFC.
.PP
To start up fv on Unix workstations, enter `fv' in a command window. You can
optionally append the name of a FITS file, or multiple files, to be
opened. For example, `fv ngc1316o.fit' opens a single file, and `fv ngc*'
opens all the FITS files in the current directory whose name matches the
string. On Windows PCs you can start fv either by double clicking on the fv
desktop icon, or by dragging a FITS file from, say, Windows Explorer onto the
fv icon. It is also possible to set up your Windows or Unix environment so
that you can just double click (in the File Manager or on an e-mail
attachment) on a FITS file that has a standard extension like `.fit' or
`.fits' and then have fv automatically start up and open that file. 
.SH OPTIONS
.PP
.TP
\fB\-cmap \fIm
Set the colormap.
.IP
\fBm=0\fR
Default behavior.  I.e. choose the "best" colormap.
.IP
\fBm=1\fR
Force POW to setup a new private pseudocolor colormap (very safe)
.IP
\fBm=2\fR
Force POW to use truecolor mode (very safe, but looks bad on low color
displays and runs slower than pseudocolor).  Note: this will cause
powSetupColormap to look for a truecolor visual; if it can't find one, it will
allow the main Tk code to pick a visual, but POW will still use "truecolor
mode" (i.e. the Tk photo widget) to display images. 
.IP
\fBm=3\fR
Force use of the screen default colormap. This should be reasonably safe now,
but often won't be what you want. 
.TP
\fB\-winmanager \fI1/0
Turn Window manager On and Off.
.TP
\fB\-quiet
Startup fv without open dialog and stay open even if all files are closed.
.TP
\fB\-module \fImodName \fR or \fB\-modName
Load a web module into fv.
.TP
\fB\-version
Display fv Version.
.TP
\fB\-user
Logon with user ID.
.SH ENVIRONMENT
The environment variable FVTMP defines what directory fv will use to create
temporary files.
.SH AUTHOR
Text copied from heasoft's web page at 
https://heasarc.gsfc.nasa.gov/docs/software/lheasoft/ftools/fv/doc/startFv.html
.SH "SEE ALSO"
The program is documented fully by the online help that can be accessed from
the built-in help viewer. 

