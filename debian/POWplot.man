.TH POWPLOT "1" "July 2009" "Heasoft" "User Commands"
.SH NAME
POWplot \- vizualize FITS data
.SH SYNOPSIS
.B POWplot filename
.SH DESCRIPTION
.PP
Through the POWplot program, FITS data can be visualized in a large variety of
styles. An interface to the SkyView online database allows for searching,
downloading, and plotting of images and object lists for a region of the sky.
.SH OPTIONS
.PP
This program does not take any command line arguments.  
.SH "SEE ALSO"
The program is documented fully by the online help that can be accessed from
the built-in help viewer.
